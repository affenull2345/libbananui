/*
 * Bananui user interface library - panel operations
 * Copyright (C) 2020-2021 Affe Null <affenull2345@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <bananui/panels.h>
#include "window_priv.h"

void bSetSoftkeys(bWindow *wnd, const char *left, const char *center,
	const char *right)
{
	char *cmd, *tmp;
	size_t cmdlen;
	if(left){
		cmdlen = 5 + strlen(left);
		cmd = malloc(cmdlen+1);
		snprintf(cmd, cmdlen+1, "skl %s\n", left);
		for(tmp = cmd; *tmp; tmp++){
			if(*tmp == '\n' && *(tmp+1) != '\0') *tmp = ' ';
		}
		write(wnd->sockfd, cmd, cmdlen);
		free(cmd);
	}
	if(center){
		cmdlen = 5 + strlen(center);
		cmd = malloc(cmdlen+1);
		snprintf(cmd, cmdlen+1, "skc %s\n", center);
		for(tmp = cmd; *tmp; tmp++){
			if(*tmp == '\n' && *(tmp+1) != '\0') *tmp = ' ';
		}
		write(wnd->sockfd, cmd, cmdlen);
		free(cmd);
	}
	if(right){
		cmdlen = 5 + strlen(right);
		cmd = malloc(cmdlen+1);
		snprintf(cmd, cmdlen+1, "skr %s\n", right);
		for(tmp = cmd; *tmp; tmp++){
			if(*tmp == '\n' && *(tmp+1) != '\0') *tmp = ' ';
		}
		write(wnd->sockfd, cmd, cmdlen);
		free(cmd);
	}
}
void bToggleHideSoftkeys(bWindow *wnd)
{
	write(wnd->sockfd, "hsp \n", 5);
}
void bToggleHideStatusbar(bWindow *wnd)
{
	write(wnd->sockfd, "htp \n", 5);
}
