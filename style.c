/*
 * Bananui user interface library - style functions
 * Copyright (C) 2020-2021 Affe Null <affenull2345@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <bananui/window.h>
#include <bananui/style.h>
#include "window_priv.h"

void bRGB(bColor *color, int r, int g, int b)
{
	color->r = r > 255 ? 255 : r;
	color->g = g > 255 ? 255 : g;
	color->b = b > 255 ? 255 : b;
}

void bSetColor(bWindow *wnd, bStyleTarget target, const char *param,
	bColor *color)
{
	char *cmd, *param_copy;
	size_t cmdsize;
	int i;
	/* sty ? ...target... [r] [g] [b]\n */
	/* r, g, and b are not longer than three characters (max 255) */
	param_copy = strdup(param);
	for(i = 0; param_copy[i]; i++){
		/* Parameter names can only contain letters or '-' */
		if(!isalpha(param_copy[i]) && param_copy[i] != '-')
		{
			param_copy[i] = '\0';
			break;
		}
	}
	cmdsize = 6 + strlen(param_copy) + 14;
	cmd = malloc(sizeof(char*) * (cmdsize));
	snprintf(cmd, cmdsize, "sty %c %s %d %d %d\n",
		(target == STYLE_TARGET_FOCUS ? 'f' :
		 target == STYLE_TARGET_SOFTKEY ? 's' :
		 target == STYLE_TARGET_TOP_PANEL ? 't' : 'w'),
		param_copy, color->r, color->g, color->b);
	write(wnd->sockfd, cmd, strlen(cmd));
	free(cmd);
	free(param_copy);
}
