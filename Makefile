LIBS = libbananui.so.0 libbananui.a
LIBSOURCES = window.c widget.c panels.c events.c device.c style.c
LIBOBJECTS = $(LIBSOURCES:.c=.o)

LIBDIR = /usr/lib
INCLUDEDIR = /usr/include

CFLAGS = -g -Wall -Wno-unused-function -Iinclude
all: $(LIBS)

libbananui.so.0: $(LIBOBJECTS)
	$(CC) -Wl,-soname,$@ -shared -o $@ $(LIBOBJECTS) $(CFLAGS)

libbananui.a: $(LIBOBJECTS)
	$(AR) rcs -o $@ $(LIBOBJECTS)

install:
	mkdir -p $(DESTDIR)$(LIBDIR)
	install $(LIBS) $(DESTDIR)$(LIBDIR)
	ln -s libbananui.so.0 $(DESTDIR)$(LIBDIR)/libbananui.so
	mkdir -p $(DESTDIR)$(INCLUDEDIR)/bananui
	install include/bananui/* $(DESTDIR)$(INCLUDEDIR)/bananui

%.o: %.c
	$(CC) $(CFLAGS) -fPIC -c $< -o $@

ifneq (clean, $(MAKECMDGOALS))
-include deps.mk
endif

deps.mk: $(LIBSOURCES)
	$(CC) -MM $^ > $@

clean:
	rm -f deps.mk *.o $(LIBS)
