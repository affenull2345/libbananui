/*
 * Bananui user interface library - device functions
 * Copyright (C) 2020-2021 Affe Null <affenull2345@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <bananui/device.h>
#include "window_priv.h"

void bVibrate(bWindow *wnd, unsigned int duration)
{
	char command[15];
	snprintf(command, sizeof(command), "vib %u\n", duration);
	write(wnd->sockfd, command, strlen(command));
}

void bSetFlashlightBrightness(bWindow *wnd, unsigned int brightness)
{
	char command[15];
	snprintf(command, sizeof(command), "fla %u\n", brightness);
	write(wnd->sockfd, command, strlen(command));
}

void bWakeup(bWindow *wnd)
{
	write(wnd->sockfd, "wak \n", 5);
}
