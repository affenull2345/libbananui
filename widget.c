/*
 * Bananui user interface library - widget creation
 * Copyright (C) 2020-2021 Affe Null <affenull2345@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <bananui/window.h>
#include <bananui/widget.h>
#include "window_priv.h"

static bWidget bCreateWidget(bWindow *wnd, bWidget replace)
{
	bWidget widget;
	if(replace >= 0){
		widget = replace;
	}
	else {
		widget = wnd->nextid++;
	}
	return widget;
}

static bWidget bCreateSingleLabel(bWindow *wnd, const char *text, bTextAlign ta,
	bWidget replace)
{
	char *cmd, alignchar;
	size_t cmdlen;
	/* Command: 'lb? ...text...\n' */
	cmdlen = 6 + strlen(text);
	if(replace >= 0){
		/* Command: 'rep [id] lb? ...text...\n' */
		/* We assume that id is less than 6 digits long */
		cmdlen += 10;
	}
	switch(ta){
		case TA_LEFT:
			alignchar = 'l';
			break;
		case TA_CENTER:
			alignchar = 'c';
			break;
		case TA_RIGHT:
			alignchar = 'r';
			break;
	}
	cmd = malloc(cmdlen);
	if(replace >= 0){
		snprintf(cmd, cmdlen, "rep %d lb%c %s\n", replace,
			alignchar, text);
	}
	else {
		snprintf(cmd, cmdlen, "lb%c %s\n", alignchar, text);
	}
	write(wnd->sockfd, cmd, strlen(cmd));
	free(cmd);
	return bCreateWidget(wnd, replace);
}

bWidget bCreateLabel(bWindow *wnd, const char *text, bTextAlign ta,
	bWidget replace)
{
	bWidget last;
	char *tmptext, *tmp, *line;
	tmp = tmptext = strdup(text);
	while(*tmp){
		line = tmp;
		for(; *tmp && *tmp != '\n'; tmp++) {}
		if(*tmp){
			*tmp = '\0';
			tmp++;
		}
		last = bCreateSingleLabel(wnd, line, ta, replace);
	}
	free(tmptext);
	return last;
}

bWidget bCreateIcon(bWindow *wnd, const char *name, bTextAlign ta,
	int width, int height, bWidget replace)
{
	char *cmd, *tmp, alignchar;
	size_t cmdlen;
	/* Command: 'ic? [w] [h] ...name...\n' */
	cmdlen = 17 + strlen(name);
	if(replace >= 0){
		/* Command: 'rep [id] ic? [w] [h] ...name...\n' */
		/* We assume that id is less than 6 digits long */
		cmdlen += 10;
	}
	switch(ta){
		case TA_LEFT:
			alignchar = 'l';
			break;
		case TA_CENTER:
			alignchar = 'c';
			break;
		case TA_RIGHT:
			alignchar = 'r';
			break;
	}
	cmd = malloc(cmdlen);
	if(replace >= 0){
		snprintf(cmd, cmdlen, "rep %d ic%c %d %d %s\n", replace,
			alignchar, width, height, name);
	}
	else {
		snprintf(cmd, cmdlen, "ic%c %d %d %s\n", alignchar,
			width, height, name);
	}
	for(tmp = cmd; *tmp; tmp++){
		if(*tmp == '\n' && *(tmp+1) != '\0') *tmp = ' ';
	}
	write(wnd->sockfd, cmd, strlen(cmd));
	free(cmd);
	return bCreateWidget(wnd, replace);
}

void bStartLine(bWindow *wnd)
{
	write(wnd->sockfd, "cbx \n", 5);
}

bWidget bEndLine(bWindow *wnd)
{
	write(wnd->sockfd, "cbe \n", 5);
	return wnd->nextid++;
}

bWidget bCreateButton(bWindow *wnd, const char *text, bWidget replace)
{
	char *cmd, *tmp;
	size_t cmdlen;
	/* Command: 'btn ...text...\n' */
	cmdlen = 6 + strlen(text);
	if(replace >= 0){
		/* Command: 'rep [id] btn ...text...\n' */
		/* We assume that id is less than 6 digits long */
		cmdlen += 10;
	}
	cmd = malloc(cmdlen);
	if(replace >= 0){
		snprintf(cmd, cmdlen, "rep %d btn %s\n", replace, text);
	}
	else {
		snprintf(cmd, cmdlen, "btn %s\n", text);
	}
	for(tmp = cmd; *tmp; tmp++){
		if(*tmp == '\n' && *(tmp+1) != '\0') *tmp = ' ';
	}
	write(wnd->sockfd, cmd, strlen(cmd));
	free(cmd);
	return bCreateWidget(wnd, replace);
}
bWidget bCreateInput(bWindow *wnd, const char *text, bInputType inptype,
	bWidget replace)
{
	char *cmd, *tmp, typechar;
	size_t cmdlen;
	/* Command: 'inp ? ...text...\n' */
	cmdlen = 8 + strlen(text);
	if(replace >= 0){
		/* Command: 'rep [id] inp ? ...text...\n' */
		/* We assume that id is less than 6 digits long */
		cmdlen += 10;
	}
	switch(inptype){
		case INPUT_TYPE_TEXT:
			typechar = 't';
			break;
		case INPUT_TYPE_PASSWORD:
			typechar = 'p';
			break;
	}
	cmd = malloc(cmdlen);
	if(replace >= 0){
		snprintf(cmd, cmdlen, "rep %d inp %c %s\n", replace,
			typechar, text);
	}
	else {
		snprintf(cmd, cmdlen, "inp %c %s\n", typechar, text);
	}
	for(tmp = cmd; *tmp; tmp++){
		if(*tmp == '\n' && *(tmp+1) != '\0') *tmp = ' ';
	}
	write(wnd->sockfd, cmd, strlen(cmd));
	free(cmd);
	return bCreateWidget(wnd, replace);
}

/* Result is returned as an event */
void bGetWidgetText(bWindow *wnd, bWidget widget)
{
	char cmd[11]; /* Command: get [id] */
	wnd->getwidget = widget;
	snprintf(cmd, 11, "get %d\n", widget);
	write(wnd->sockfd, cmd, strlen(cmd));
}
