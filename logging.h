/*
 * Bananui user interface library - logging
 * Copyright (C) 2020-2021 Affe Null <affenull2345@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef _BANANUI_LOGGING_H_
#define _BANANUI_LOGGING_H_

#include <stdio.h>
#include <errno.h>

static void bananui_log(const char *libname, const char *msgtype,
	const char *function, const char *message, int show_errno)
{
	if(show_errno){
		fprintf(stderr, "%s: %s: %s: %s: %s\n", libname, function,
			msgtype, message, strerror(errno));
	}
	else {
		fprintf(stderr, "%s: %s: %s: %s\n", libname, function,
			msgtype, message);
	}
}

#define LOG_FAILURE(_a, _e) bananui_log("libbananui", "FAIL", \
				__FUNCTION__, _a, _e)

#endif
