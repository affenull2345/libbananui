/*
 * Bananui user interface library - widget header for libbananui
 * Copyright (C) 2020-2021 Affe Null <affenull2345@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef _BANANUI_WIDGET_H_
#define _BANANUI_WIDGET_H_

#define NO_WIDGET -1

#include <bananui/window.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef int bWidget;
typedef enum bananui_text_align {
	TA_LEFT,
	TA_CENTER,
	TA_RIGHT
} bTextAlign;

typedef enum bananui_input_type {
	INPUT_TYPE_TEXT,
	INPUT_TYPE_PASSWORD
} bInputType;

/* Set replace to NO_WIDGET if you want to add a widget at the end */

bWidget bCreateButton(bWindow *wnd, const char *txt, bWidget replace);
bWidget bCreateLabel(bWindow *wnd, const char *txt, bTextAlign align,
	bWidget replace);
bWidget bCreateIcon(bWindow *wnd, const char *name, bTextAlign align,
	int width, int height, bWidget replace);
bWidget bCreateInput(bWindow *wnd, const char *txt, bInputType inptype,
	bWidget replace);

void bStartLine(bWindow *wnd);
bWidget bEndLine(bWindow *wnd);

/* Returns the text in a BEV_SET event */
void bGetWidgetText(bWindow *wnd, bWidget widget);

#ifdef __cplusplus
}
#endif

#endif
