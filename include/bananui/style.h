/*
 * Bananui user interface library - style header for libbananui
 * Copyright (C) 2020-2021 Affe Null <affenull2345@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef _BANANUI_STYLE_H_
#define _BANANUI_STYLE_H_

typedef struct bananui_color {
	int r, g, b; /* DO NOT USE - use bRGB instead */
} bColor;

typedef enum bananui_style_target {
	STYLE_TARGET_WINDOW_WIDGET,
	STYLE_TARGET_FOCUS,
	STYLE_TARGET_SOFTKEY,
	STYLE_TARGET_TOP_PANEL
} bStyleTarget;

void bRGB(bColor *color, int r, int g, int b);

void bSetColor(bWindow *wnd, bStyleTarget target, const char *param,
	bColor *color);

#endif
