/*
 * Bananui user interface library - window header for libbananui
 * Copyright (C) 2020-2021 Affe Null <affenull2345@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef _BANANUI_WINDOW_H_
#define _BANANUI_WINDOW_H_

#ifdef __cplusplus
extern "C" {
#endif

typedef struct bananui_window bWindow;

bWindow *bCreateWnd();

/* Only needed if there is no event loop */
void bCloseWnd(bWindow *wnd);

void bSetWindowTitle(bWindow *wnd, const char *title);

void bClearWnd(bWindow *wnd);
void bRefreshWnd(bWindow *wnd, int smooth);
void *bSetWindowData(bWindow *wnd, void *data);
void *bGetWindowData(bWindow *wnd);

#ifdef __cplusplus
}
#endif

#endif
