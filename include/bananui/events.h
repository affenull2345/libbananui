/*
 * Bananui user interface library - event header for libbananui
 * Copyright (C) 2020-2021 Affe Null <affenull2345@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef _BANANUI_EVENTS_H_
#define _BANANUI_EVENTS_H_

#include <stddef.h>
#include <bananui/window.h>
#include <bananui/widget.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef enum bananui_event_type {
	BEV_KEYUP,
	BEV_KEYDOWN,
	BEV_CLICK,
	BEV_FOCUS,
	BEV_EXIT,
	BEV_SET
} bEventType;
typedef struct {
	int val;
	bWidget widget; /* For convenience (it's actually just the ID) */
	bWindow *window;
	char *text; /* Only works with BEV_SET */
} bEventData;

/* Should return exit code or -1 if no need to exit */
typedef int (*event_callback) (bEventType, bEventData*);

event_callback bSetEventCallback(bWindow *win, event_callback cb);

/* numfds should be set to the size of the fds array and is changed by
 * the function to the actual number of file descriptors */
int bGetConnectionFds(bWindow *win, int *fds, size_t *numfds);

/* Returns exit code or -1 if no need to exit */
int bHandleResponse(bWindow *wnd, int fd);

int bEventLoop(bWindow *win);

#ifdef __cplusplus
}
#endif

#endif
