/*
 * Bananui user interface library - main header for libbananui
 * Copyright (C) 2020-2021 Affe Null <affenull2345@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef _BANANUI_H_
#define _BANANUI_H_

/* This file just includes all other useful files */

#include <bananui/window.h>
#include <bananui/widget.h>
#include <bananui/style.h>
#include <bananui/panels.h>
#include <bananui/events.h>
#include <bananui/device.h>

#endif
