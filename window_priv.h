/*
 * Bananui user interface library - private header for window.c
 * Copyright (C) 2020-2021 Affe Null <affenull2345@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef _BANANUI_WINDOW_PRIV_H_
#define _BANANUI_WINDOW_PRIV_H_

#define RESPONSE_BUFFER_SIZE 1024

#include <bananui/events.h>

struct bananui_window {
	int sockfd;
	int nextid;
	event_callback cb;
	void *userdata;
	char buf[RESPONSE_BUFFER_SIZE];
	int bufindex;
	int getwidget;
};

#endif
