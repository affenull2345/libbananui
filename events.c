/*
 * Bananui user interface library - event loop and handlers.
 * Copyright (C) 2020-2021 Affe Null <affenull2345@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/select.h>
#include <unistd.h>
#include <bananui/events.h>
#include <bananui/window.h>
#include "window_priv.h"
#include "logging.h"


event_callback bSetEventCallback(bWindow *wnd, event_callback cb)
{
	event_callback prevcb = wnd->cb;
	wnd->cb = cb;
	return prevcb;
}

int bGetConnectionFds(bWindow *wnd, int *fds, size_t *numfds)
{
	if(*numfds < 1) return -1;
	fds[0] = wnd->sockfd;
	*numfds = 1;
	return 0;
}

/* Returns exit code or -1 if no need to exit */
int bHandleResponse(bWindow *wnd, int fd)
{
	int res;
	char ch;
	if(read(fd, &ch, 1) < 1){
		LOG_FAILURE("Could not read from server", 0);
		return 1;
	}
	if(ch == '\r') return -1;
	if(ch == '\n' || wnd->bufindex >= RESPONSE_BUFFER_SIZE-1){
		bEventType type;
		bEventData data;
		data.window = wnd;
		wnd->buf[wnd->bufindex] = '\0';
		wnd->bufindex = 0;
		if(0 == strncmp(wnd->buf, "kup ", 4)) type=BEV_KEYUP;
		else if(0 == strncmp(wnd->buf, "kdn ", 4)) type=BEV_KEYDOWN;
		else if(0 == strncmp(wnd->buf, "clk ", 4)) type=BEV_CLICK;
		else if(0 == strncmp(wnd->buf, "foc ", 4)) type=BEV_FOCUS;
		else if(0 == strncmp(wnd->buf, "exi ", 4)) type=BEV_EXIT;
		else if(0 == strncmp(wnd->buf, "set ", 4)) type=BEV_SET;
		if(type != BEV_SET)
			data.val = strtol(wnd->buf+4, NULL, 10);
		if(type == BEV_CLICK || type == BEV_FOCUS)
			data.widget = data.val;
		if(type == BEV_SET){
			data.widget = data.val = wnd->getwidget;
			data.text = wnd->buf+4;
		}
		if(wnd->cb && (res = wnd->cb(type, &data)) != -1){
			bCloseWnd(wnd);
			return res;
		}
	}
	else {
		wnd->buf[wnd->bufindex] = ch;
		wnd->bufindex++;
	}
	return -1;
}

int bEventLoop(bWindow *wnd)
{
	fd_set readfds;
	int fds[1];
	int res;
	size_t nfds = 1;
	while(1){
		FD_ZERO(&readfds);
		/* nfds should stay 1 */
		bGetConnectionFds(wnd, fds, &nfds);
		FD_SET(fds[0], &readfds);
		res = select(fds[0]+1, &readfds, NULL, NULL, NULL);
		if(res <= 0){
			perror("select");
			return 1;
		}
		if((res = bHandleResponse(wnd, fds[0])) >= 0){
			return res;
		}
	}
}
