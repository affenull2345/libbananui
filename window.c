/*
 * Bananui user interface library - window operations
 * Copyright (C) 2020-2021 Affe Null <affenull2345@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <bananui/window.h>
#include "logging.h"
#include "window_priv.h"

#define BANANUI_SOCKET_FILE "/tmp/bananui.sock"

bWindow *bCreateWnd()
{
	struct sockaddr_un addr = {AF_UNIX, BANANUI_SOCKET_FILE};
	bWindow *wnd;
	wnd = malloc(sizeof(bWindow));
	wnd->sockfd = socket(AF_UNIX, SOCK_STREAM, 0);
	if(wnd->sockfd < 0){
		LOG_FAILURE("socket", 1);
		free(wnd);
		return NULL;
	}
	if(connect(wnd->sockfd, (struct sockaddr*) &addr, sizeof(addr)) < 0){
		LOG_FAILURE("Failed to bind to " BANANUI_SOCKET_FILE, 1);
		free(wnd);
		return NULL;
	}
	fcntl(wnd->sockfd, F_SETFD, FD_CLOEXEC);
	wnd->nextid = 0;
	wnd->cb = NULL;
	wnd->bufindex = 0;
	return wnd;
}

void bCloseWnd(bWindow *wnd)
{
	close(wnd->sockfd);
	free(wnd);
}

void bRefreshWnd(bWindow *wnd, int smooth)
{
	if(smooth){
		write(wnd->sockfd, "rfr c\n", 6);
	}
	else {
		write(wnd->sockfd, "rfr \n", 5);
	}
}

void bSetWindowTitle(bWindow *wnd, const char *title)
{
	char *cmd, *tmp;
	size_t cmdlen;
	cmdlen = 5 + strlen(title);
	cmd = malloc(cmdlen+1);
	snprintf(cmd, cmdlen+1, "tit %s\n", title);
	for(tmp = cmd; *tmp; tmp++){
		if(*tmp == '\n' && *(tmp+1) != '\0') *tmp = ' ';
	}
	write(wnd->sockfd, cmd, cmdlen);
	free(cmd);
}

void bClearWnd(bWindow *wnd)
{
	write(wnd->sockfd, "clr \n", 5);
	wnd->nextid = 0;
}

void *bGetWindowData(bWindow *wnd)
{
	return wnd->userdata;
}

void *bSetWindowData(bWindow *wnd, void *data)
{
	void *prev = wnd->userdata;
	wnd->userdata = data;
	return prev;
}
